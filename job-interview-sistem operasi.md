# 1. Mendemonstrasikan penggunaan Sistem Operasi untuk

- Monitoring resource

**RAM**
memori jangka pendek komputer atau laptop
untuk memonitor RAM dapat menggunakan command

- **free**
![dokumentasiso](dokumentasiso/free-mggg.jpeg)

dan untuk lebih rinci kita dapat menggunakan command --mega (megabyte) atau --giga (gigabyte)
untuk tampilan command **free --mega** dan **free --giga**

![dokumentasiso](dokumentasiso/free-mggg.jpeg)

CPU
yaitu prosesor yang dapat bertanggung jawab untuk menjalankan instruksi pada perangkat komputasi
untuk memonitor CPU dapat menggunakan command **top** atau **ps -aux**
untuk tampilan **ps -aux**

![dokumentasiso](dokumentasiso/ps-aux.jpeg)

dan tampilan untuk penggunaan command **top**

![dokumentasiso](dokumentasiso/top.jpeg)

**Hardisk**
yaitu sebuah perangkat keras yang mempunyai fungsi untuk menyimpan sebuah data.
Untuk memonitor Hardisk di MobaXtrem kita dapat menggunakan command **df** atau **lsblk** 
Untuk tampilan **df**

![dokumentasiso](dokumentasiso/df.jpeg)

dan untuk tampilan command **lsblk**

![dokumentasiso](dokumentasiso/lslbk.jpeg)


# 2. Manajemen Program

- Memonitor Program yang sedang berjalan

untuk memonitor program yang sedang berjalan dapat menggunakan command **top** atau **ps -aux**

Untuk tampilan command **top**

![dokumentasiso](dokumentasiso/top.jpeg)

# 3. Manajemen Network
- Mengakses Sistem Operasi Menggunakan SSH

SSH client adalah aplikasi yang dipakai untuk menghubungkan sistem operasi dengan SSH server.
Untuk akses menggunakan SSH yaitu dengan perintah berikut:

![dokumentasiso](dokumentasiso/ssh.jpeg)

- Memonitor Program Menggunakan Network

Untuk dapat memonitor Network kita perlu memasukan perintah netstat

![dokumentasiso](dokumentasiso/netstat.jpeg)

- Mengoperasikan HTTP client
adalah perangkat lunak yang terutama digunakan untuk memungkinkan kita mengunduh file dari Internet. 

Untuk mengoperasikan HTTP client yaitu dengan menggunakan perintah curl atau wget.

![dokumentasiso](dokumentasiso/wegett.jpeg)

`curl`

Curl command adalah command yang tersedia di sebagian besar sistem berbasis Unix. Curl merupakan singkatan dari “Client URL”.

![dokumentasiso](dokumentasiso/curl.jpeg)


# 4. Manajemen File Dan folder
Navigasi

Untuk navigasi bisa menggunakan command berikut:

- cd [Nama Folder]


- ls


- pwd

`cd`, untuk mengganti direktori
`ls`, menampilkan semua list file dan folder yang ada di direktori yang sedang ditempati
`pwd`, menampilkan direktori yang sedang ditempati

![dokumentasiso](dokumentasiso/cd.jpeg)

Crud File Dan Folder

Untuk CRUD bisa menggunakan command berikut:

- mkdir [Nama Direktori]


- touch [Nama File]


- rm  [Nama File]


- rmdir [Nama Folder]


- mv  [Nama File/Direktori] [Tujuan Direktori]


- cp  [Nama File/Direktori] [Tujuan Direktori]

mkdir, untuk membuat direktori
touch, untuk membuat file
rm, menghapus file
rmdir, menghapus direktori
mv, cut/move file atau direktori
cp, copy file atau direktori

![dokumentasiso](dokumentasiso/mv.jpeg)


- Manajemen ownership
Ada dua command dasar dalam manajemen ownership yaitu chmod dan chown.

CHMOD adalah kependekan dari CHange MODe, adalah merubah mode akses dari suatu file atau direktori
adalah contoh penggunaannya:

- chmod, digunakan untuk memodifikasi permission suatu file.
[option] berisi perintah untuk mengubah permission
<file> berisi nama file yang akan diubah permissionnya.
ls -l (berfungsi sebgai melihat hak akses dari file dan folder )

- Chown digunakan untuk mengubah pemilik dan grup pada sebuah file dan folder

chown [user:group] <file>

![dokumentasiso](dokumentasiso/chmod.jpeg)


- pencarian File Dan Folder

Untuk mencari file dan folder bisa menggunakan find atau grep. Contoh penggunaan find:

`find <path> <conditions> <actions>`

![dokumentasiso](dokumentasiso/find.jpeg)


`<directory>,` diisi dengan memasukkan direktori yang akan dicari file dan foldernya.
`<conditions>`, diisi dengan kondisi yang akan dipilih.
`<actions>`, diisi dengan actions yang akan dilakukan.

- Kompresi Data

Kompresi data atau pemampatan data adalah sebuah cara dalam ilmu komputer untuk memadatkan data sehingga hanya memerlukan ruangan penyimpanan lebih kecil.
Untuk melakukan kompresi data, bisa menggunakan command tar dan gzip, berikut contoh
penggunaanya:

`Tar -cf (untuk membuat file.tar)`

`Tar -xf (untuk membuka file.tar)`

Dengan contoh untuk membuat file tar

`tar [-cf] [fileku.tar] [file1 file2]`

![dokumentasiso](dokumentasiso/tarcf.jpeg)

untuk membuka file tar tersebut gunakan command

`Tar -xf [fileku.tar] [file1 file2]`

![dokumentasiso](dokumentasiso/tarxf.jpeg)

- Zip merupakan format file arsip yang digunakan secara luas untuk mengompresi atau memampatkan satu atau beberapa file bersama-sama menjadi ke dalam satu lokasi

![dokumentasiso](dokumentasiso/gzip.jpeg)

Untuk Mengekstrak file zip tersebut dapat menggunakan command

`gunzip [options] [filenames]`

![dokumentasiso](dokumentasiso/gunzip.jpeg)

# 5. Membuat Program Berbasis CLI Menggunakan ShellScript

Langkah pertama membuat file.sh terlebih dahulu dengan command

![dokumentasiso](dokumentasiso/touch.jpeg)

Selanjutnya kita masuk ke mode vim dengan menggunakan command vim lalu inputkan command sesuai kebutuhan.

![dokumentasiso](dokumentasiso/clii.jpeg)

elanjutnya Kita eksekusi program tersebut dengan menggunakan command

`./nama-file`

an jika muncul notif "permission denied" maka kita harus mengubah hak akses file tersebut dengan melihat hak akses menggunakan command ls -l (melihat hak akses)
an command chmod untuk merubah hak akses file tersebut.

`chmod [option] [nama file]`


Selanjutnya kita eksekusi kembali dan inputkan sesuai dengan programnya dengan command

`./nama-file`

![dokumentasiso](dokumentasiso/clio.jpeg)

# 6. Demonstrasi program CLI dalam bentuk Youtube


# 7. Maze Challenge

![dokumentasiso](dokumentasiso/maze1.jpeg)

![dokumentasiso](dokumentasiso/maze2.jpeg)

![dokumentasiso](dokumentasiso/maze3.jpeg)

![dokumentasiso](dokumentasiso/maze4.jpeg)

![dokumentasiso](dokumentasiso/maze5.jpeg)

![dokumentasiso](dokumentasiso/maze6.jpeg)

![dokumentasiso](dokumentasiso/maze7.jpeg)
